﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelCode
{
    class MagicAddition
    {
        private Stopwatch _sw;
        private int _magicIntOne;
        private int _magicIntTwo;

        public MagicAddition()
        {
            _sw = new Stopwatch();
            _magicIntOne = 0;
            _magicIntTwo = 0;
        }

        public void PrintMagicInts()
        {
            Console.WriteLine("Magic Int One: " + _magicIntOne);
            Console.WriteLine("Magic Int Two: " + _magicIntTwo);
            Console.WriteLine("Take Taken: " + _sw.ElapsedMilliseconds + "ms");
        }

        public void NoConcurrency(int iterations)
        {
            _sw.Start();
            for (int i = 0; i < iterations; i++)
            {
                _magicIntOne++;
                _magicIntTwo += 2;
            }
            _sw.Stop();
        }

        public void StupidConcurrency(int iterations)
        {
            _sw.Start();

            int processorCount = Environment.ProcessorCount;
            int paritionSize = iterations / processorCount;
            int remainder = iterations % processorCount;
           
            Action<object> action = new Action<object>((count) =>
            {
                for (int j = (int)count - paritionSize; j < (int)count; j++)
                {
                    _magicIntOne++;
                    _magicIntTwo += 2;
                }
            });

            if (remainder != 0)
                Task.Factory.StartNew(action, remainder);

            for (int i = 0; i < processorCount; i++)
            {
                int maxNumber = paritionSize * i;

                Task.Factory.StartNew(action, maxNumber);
            }

            _sw.Stop();
        }

        public void StupidParallelFor(int iterations)
        {
            _sw.Start();

            Parallel.For(0, iterations, (i) =>
            {
                _magicIntOne++;
                _magicIntTwo += 2;
            });

            _sw.Stop();
        }

        public void NastyLocks(int iterations)
        {
            _sw.Start();

            Object lockObjOne = new Object();
            Object lockObjTwo = new Object();

            Parallel.For(0, iterations, (i) =>
            {
                lock (lockObjOne)
                {
                    _magicIntOne++;
                }

                lock (lockObjTwo)
                { 
                    _magicIntTwo += 2;
                }
            });

            _sw.Stop();
        }


        internal void LockFreeAtomicOperations(int iterations)
        {
            _sw.Start();

            int processorCount = Environment.ProcessorCount;
            int paritionSize = iterations / processorCount;
            int remainder = iterations % processorCount;
            List<Task> tasks = new List<Task>();

            Action<object> action = new Action<object>((count) =>
            {
                int magicIntOne = 0;
                int magicIntTwo = 0;

                for (int j = (int)count - paritionSize; j < (int)count; j++)
                {
                    magicIntOne++;
                    magicIntTwo += 2;
                }

                Interlocked.Add(ref _magicIntOne, magicIntOne);
                Interlocked.Add(ref _magicIntTwo, magicIntTwo);
            });

            if (remainder != 0)
                tasks.Add(Task.Factory.StartNew(action, remainder));

            for (int i = 0; i < processorCount; i++)
            {
                int maxNumber = paritionSize * i;

                tasks.Add(Task.Factory.StartNew(action, maxNumber));
            }

            Task.WaitAll(tasks.ToArray());

            _sw.Stop();
        }

        internal void LockFreeLocalStorage(int iterations)
        {
            _sw.Start();

            int processorCount = Environment.ProcessorCount;
            int paritionSize = iterations / processorCount;
            int remainder = iterations % processorCount;
            List<Task<int[]>> tasks = new List<Task<int[]>>();

            Func<object, int[]> action = new Func<object, int[]>((count) =>
            {
                int[] integers = new int[2];

                for (int j = (int)count - paritionSize; j < (int)count; j++)
                {
                    integers[0]++;
                    integers[1] += 2;
                }

                return integers;
            });

            if (remainder != 0)
                tasks.Add(Task.Factory.StartNew(action, remainder));

            for (int i = 0; i < processorCount; i++)
            {
                int maxNumber = paritionSize * i;

                tasks.Add(Task.Factory.StartNew(action, maxNumber));
            }

            Task.WaitAll(tasks.ToArray());

            foreach (Task<int[]> t in tasks)
            {
                _magicIntOne += t.Result[0];
                _magicIntTwo += t.Result[1];
            }

            _sw.Stop();
        }
    }
}
